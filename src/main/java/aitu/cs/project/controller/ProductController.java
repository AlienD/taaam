package aitu.cs.project.controller;

import aitu.cs.project.model.OrderItem;
import aitu.cs.project.model.Product;
import aitu.cs.project.service.ProductService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class ProductController {
    private final ProductService productService;

    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping("/product")
    public ResponseEntity<?> getAll(){
        return ResponseEntity.ok(productService.getAll());
    }


    @GetMapping("/product/{id}")
    public ResponseEntity<?> getByID(@PathVariable int id){
        return productService.getByID(id);
    }

    @DeleteMapping("/product/{id}")
    public void deleteByID(@PathVariable int id){
        productService.deleteByID(id);
    }

    @PostMapping("/product")
    public ResponseEntity<?> create(@RequestBody Product product){
        return ResponseEntity.ok(productService.save(product));
    }

    @GetMapping("/product/category/{id}")
    public ResponseEntity<?> getByCategory(@PathVariable int id){
        return ResponseEntity.ok(productService.getByCategoryID(id));
    }

    @PostMapping("/add/product/{id}")
    public ResponseEntity<?> addProduct(@PathVariable int id){
        return ResponseEntity.ok(productService.addProduct(id));
    }
}
