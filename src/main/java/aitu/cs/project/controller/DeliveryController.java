package aitu.cs.project.controller;

import aitu.cs.project.model.CustomerOrder;
import aitu.cs.project.model.Delivery;
import aitu.cs.project.service.DeliveryService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class DeliveryController {
    private final DeliveryService deliveryService;

    public DeliveryController(DeliveryService deliveryService) {
        this.deliveryService = deliveryService;
    }

    @GetMapping("/delivery")
    public ResponseEntity<?> getAll(){
        return deliveryService.getAll();
    }

    @GetMapping("/delivery/{id}")
    public ResponseEntity<?> getByID(@PathVariable int id){
        return deliveryService.getByID(id);
    }

    @DeleteMapping("/delivery/{id}")
    public void deleteByID(@PathVariable int id){
        deliveryService.deleteByID(id);
    }

    @PostMapping("/delivery")
    public ResponseEntity<?> create(@RequestBody Delivery delivery){
        return ResponseEntity.ok(deliveryService.save(delivery));
    }
}
