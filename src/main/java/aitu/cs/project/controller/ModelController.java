package aitu.cs.project.controller;

import aitu.cs.project.model.Factory;
import aitu.cs.project.model.Model;
import aitu.cs.project.service.ModelService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class ModelController {
    private final ModelService modelService;

    public ModelController(ModelService modelService) {
        this.modelService = modelService;
    }

    @GetMapping("/model")
    public ResponseEntity<?> getAll(){
        return modelService.getAll();
    }

    @GetMapping("/model/{id}")
    public ResponseEntity<?> getByID(@PathVariable int id){
        return modelService.getByID(id);
    }

    @DeleteMapping("/model/{id}")
    public void deleteByID(@PathVariable int id){
        modelService.deleteByID(id);
    }

    @PostMapping("/model")
    public ResponseEntity<?> create(@RequestBody Model model){
        return ResponseEntity.ok(modelService.save(model));
    }
}
