package aitu.cs.project.repository;

import aitu.cs.project.model.Delivery;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DeliveryRepository extends CrudRepository<Delivery, Integer> {
    Delivery findByDeliveryId(int id);
}
