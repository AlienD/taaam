package aitu.cs.project.repository;

import aitu.cs.project.model.Account;
import aitu.cs.project.model.CustomerOrder;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;

@Repository
public interface CustomerOrderRepository extends CrudRepository<CustomerOrder, Integer> {
    @Transactional
    @Modifying
    @Query(
            value = "insert into customerorder (customer_id, delivery_id, payment_method, date, total_price, status, token) values (:customer_id, :delivery_id, :payment_method, :date, :total_price, :status, :token)",
            nativeQuery = true)
    void insertCustomerOrder(@Param("customer_id") int customerId, @Param("delivery_id") int deliveryId,
                             @Param("payment_method") String paymentMethod, @Param("date") LocalDate date, @Param("total_price") float totalPrice, @Param("status") String status, @Param("token") String token);


    CustomerOrder findByToken(String token);

    List<CustomerOrder> findByCustomerId(int customerId);

    List<CustomerOrder> findByStatus(String status);
}
