package aitu.cs.project.repository;

import aitu.cs.project.model.Account;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.time.LocalDate;

@Repository
public interface AccountRepository extends CrudRepository<Account, Integer> {
    Account findByLoginAndPassword(String login, String password);

    Account findByToken(String token);

    @Transactional
    @Modifying
    @Query(
            value = "insert into account (role, login, password, token) values (:role, :login, :password, :token)",
            nativeQuery = true)
    void insertAccount(@Param("role") String role, @Param("login") String login,
                             @Param("password") String password, @Param("token") String token);

}
