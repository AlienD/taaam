package aitu.cs.project.service;

import aitu.cs.project.model.Account;
import aitu.cs.project.model.Customer;
import aitu.cs.project.repository.AccountRepository;
import aitu.cs.project.repository.CustomerRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class CustomerService {
    private final CustomerRepository customerRepository;
    private final AccountRepository accountRepository;

    public CustomerService(CustomerRepository customerRepository, AccountRepository accountRepository) {
        this.customerRepository = customerRepository;
        this.accountRepository = accountRepository;
    }

    public ResponseEntity<?> getAll(){
        return ResponseEntity.ok(customerRepository.findAll());
    }

    public ResponseEntity<?> getByID(int id){
        return ResponseEntity.ok(customerRepository.findById(id));
    }

    public void deleteByID(int id){
        customerRepository.deleteById(id);
    }

    public Customer save(Customer customer){
        return customerRepository.save(customer);
    }

    public Customer getCustomerByToken(String token) {
        Account account = accountRepository.findByToken(token);
        return customerRepository.findByAccountId(account.getAccountId());
    }

    public Customer register(String token, Customer customer) {
        Account account = accountRepository.findByToken(token);
        String organizationName = customer.getOrganizationName();
        String address = customer.getAddress();
        String contactNumber = customer.getContactNumber();
        String email = customer.getEmail();
        int discount = 0;
        customerRepository.insertCustomer(account.getAccountId(), organizationName, address, contactNumber, email, discount);
        return customerRepository.findByAccountId(account.getAccountId());
    }
}
