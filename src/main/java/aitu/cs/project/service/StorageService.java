package aitu.cs.project.service;

import aitu.cs.project.model.Factory;
import aitu.cs.project.model.Storage;
import aitu.cs.project.repository.StorageRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class StorageService {
    private final StorageRepository storageRepository;

    public StorageService(StorageRepository storageRepository) {
        this.storageRepository = storageRepository;
    }

    public ResponseEntity<?> getAll(){
        return ResponseEntity.ok(storageRepository.findAll());
    }

    public ResponseEntity<?> getByID(int id){
        return ResponseEntity.ok(storageRepository.findById(id));
    }

    public void deleteByID(int id){
        storageRepository.deleteById(id);
    }

    public Storage save(Storage storage){
        return storageRepository.save(storage);
    }
}
