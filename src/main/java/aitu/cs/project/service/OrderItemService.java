package aitu.cs.project.service;

import aitu.cs.project.model.CustomerOrder;
import aitu.cs.project.model.OrderItem;
import aitu.cs.project.repository.CustomerOrderRepository;
import aitu.cs.project.repository.OrderItemRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
public class OrderItemService {
    private final OrderItemRepository orderItemRepository;
    private final CustomerOrderRepository customerOrderRepository;


    public OrderItemService(OrderItemRepository orderItemRepository, CustomerOrderRepository customerOrderRepository) {
        this.orderItemRepository = orderItemRepository;
        this.customerOrderRepository = customerOrderRepository;

    }

    public ResponseEntity<?> getAll(){
        return ResponseEntity.ok(orderItemRepository.findAll());
    }

    public ResponseEntity<?> getByID(int id){
        return ResponseEntity.ok(orderItemRepository.findById(id));
    }

    public void deleteByID(int id){
        orderItemRepository.deleteById(id);
    }

    public OrderItem save(OrderItem orderItem){
        return orderItemRepository.save(orderItem);
    }

    @Transactional
    public List<CustomerOrder> getByCustomerID(Integer customerID) {
        List<CustomerOrder> orders = customerOrderRepository.findByCustomerId(customerID);

        for(CustomerOrder order: orders) {
            order.setOrderItems(orderItemRepository.findByOrderId(order.getId()));
        }

        return orders;
    }
}
