package aitu.cs.project.service;

import aitu.cs.project.model.Delivery;
import aitu.cs.project.model.Employee;
import aitu.cs.project.repository.EmployeeRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class EmployeeService {
    private final EmployeeRepository employeeRepository;

    public EmployeeService(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    public ResponseEntity<?> getAll(){
        return ResponseEntity.ok(employeeRepository.findAll());
    }

    public ResponseEntity<?> getByID(int id){
        return ResponseEntity.ok(employeeRepository.findById(id));
    }

    public void deleteByID(int id){
        employeeRepository.deleteById(id);
    }

    public Employee save(Employee employee){
        return employeeRepository.save(employee);
    }
}
