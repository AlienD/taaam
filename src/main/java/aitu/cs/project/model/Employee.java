package aitu.cs.project.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@Entity
@Table(name = "Employee")
public class Employee {
    @Id
    private int employeeId;
    private int accountId;
    private int factoryId;
    private int specialityId;
    private String name;
    private String surname;
    private String email;
    private String phoneNumber;
    private float salary;
}
