DROP TABLE IF EXISTS Customer CASCADE;
CREATE TABLE Customer (
                            "customer_id" serial NOT NULL,
                            "account_id" integer NOT NULL,
                            "organization_name" varchar(255) NOT NULL,
                            "address" varchar(255) NOT NULL,
                            "contact_number" varchar(11) NOT NULL,
                            "email" varchar(255) NOT NULL,
                            "discount" float,
                            CONSTRAINT "Customer_pk" PRIMARY KEY ("customer_id")

    );


DROP TABLE IF EXISTS Account CASCADE;
CREATE TABLE Account (
                           "account_id" serial NOT NULL,
                           "role" varchar(255) NOT NULL,
                           "login" varchar(255) NOT NULL UNIQUE,
                           "password" varchar(255) NOT NULL,
                           "token" varchar(255) default null,
                           CONSTRAINT "Account_pk" PRIMARY KEY ("account_id")

    );


DROP TABLE IF EXISTS CustomerOrder CASCADE;
CREATE TABLE CustomerOrder (
                                 "id" serial NOT NULL,
                                 "customer_id" integer NOT NULL,
                                 "delivery_id" integer NOT NULL,
                                 "payment_method" varchar(255) NOT NULL,
                                 "date" date NOT NULL,
                                 "total_price" FLOAT NOT NULL,
                                 "status" varchar(255) NOT NULL,
                                 "token" varchar(255) default null,
                                 CONSTRAINT "CustomerOrder_pk" PRIMARY KEY ("id")

    );


DROP TABLE IF EXISTS OrderItem CASCADE;
CREATE TABLE OrderItem (
                             "id" serial NOT NULL,
                             "order_id" integer NOT NULL,
                             "product_id" integer NOT NULL,
                             "quantity" integer NOT NULL,
                             "price" FLOAT NOT NULL,
                             CONSTRAINT "OrderItem_pk" PRIMARY KEY ("id")
    );


DROP TABLE IF EXISTS Product CASCADE;
CREATE TABLE Product (
                           "product_id" serial NOT NULL,
                           "model_id" integer NOT NULL,
                           "factory_id" integer NOT NULL,
                           "product_name" varchar(255) NOT NULL,
                           "product_description" varchar(255) NOT NULL,
                           "price" FLOAT NOT NULL,
                           "quantity" integer NOT NULL,
                           "creation_date" varchar NOT NULL,
                           CONSTRAINT "Product_pk" PRIMARY KEY ("product_id")
    );


DROP TABLE IF EXISTS Model CASCADE;
CREATE TABLE Model (
                         "model_id" serial NOT NULL,
                         "category_id" integer NOT NULL,
                         "model_name" varchar(255) NOT NULL,
                         "serial_number" varchar(255) NOT NULL,
                         CONSTRAINT "Model_pk" PRIMARY KEY ("model_id")
    );


DROP TABLE IF EXISTS Category CASCADE;
CREATE TABLE Category (
                            "category_id" serial,
                            "storage_id" integer NOT NULL,
                            "category_name" varchar(255) NOT NULL,
                            CONSTRAINT "Category_pk" PRIMARY KEY ("category_id")
    );


DROP TABLE IF EXISTS Employee CASCADE;
CREATE TABLE Employee (
                            "employee_id" serial NOT NULL,
                            "account_id" integer NOT NULL,
                            "factory_id" integer NOT NULL,
                            "speciality_id" integer NOT NULL,
                            "name" varchar(255) NOT NULL,
                            "surname" varchar(255) NOT NULL,
                            "email" varchar(255) NOT NULL,
                            "phone_number" varchar(11) NOT NULL,
                            "salary" FLOAT NOT NULL,
                            CONSTRAINT "Employee_pk" PRIMARY KEY ("employee_id")
    );


DROP TABLE IF EXISTS Storage CASCADE;
CREATE TABLE Storage (
                           "storage_id" serial NOT NULL,
                           "address" varchar(255) NOT NULL,
                           CONSTRAINT "Storage_pk" PRIMARY KEY ("storage_id")

    );


DROP TABLE IF EXISTS Delivery CASCADE;
CREATE TABLE Delivery (
                            "delivery_id" serial NOT NULL,
                            "type" varchar(255) NOT NULL,
                            "price" FLOAT NOT NULL,
                            CONSTRAINT "Delivery_pk" PRIMARY KEY ("delivery_id")

    );


DROP TABLE IF EXISTS Factory CASCADE;
CREATE TABLE Factory (
                           "factory_id" serial NOT NULL,
                           "factory_name" varchar(255) NOT NULL,
                           "address" varchar(255) NOT NULL,
                           CONSTRAINT "Factory_pk" PRIMARY KEY ("factory_id")
    );
