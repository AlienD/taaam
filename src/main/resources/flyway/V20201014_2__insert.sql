

INSERT INTO Storage (storage_id, address)
VALUES (1, 'Qabanbay Batyra st.47'),
       (2, 'Mangilik st.13'),
       (3, 'Turan st.7'),
       (4, 'Bogenbay Batyra st.47'),
       (5, 'Azret Sultan st.47');

INSERT INTO Delivery (delivery_id, type, price)
VALUES (1, 'courier', 10),
       (2, 'pickup', 0);


INSERT INTO Category (category_id, storage_id, category_name)
VALUES (1, 2, 'RAM'),
       (2, 1, 'CPU'),
       (3, 2, 'GPU'),
       (4, 4, 'SSD'),
       (5, 3, 'HDD'),
       (6, 1, 'Motherboard'),
       (7, 5, 'Power supply'),
       (8, 5, 'Fan');

INSERT INTO Factory (factory_name, address)
VALUES ('Libre systems', 'Aq Orda st. 1');

insert into Model values (1,1,'Ryzen 5 3600', '5580');
insert into Model values (2,1,'Ryzen 3 3100', '5673');
insert into model values (3,1,'Ryzen 7 3700X', '5583');
insert into model values (4,1,'A8-7680', '9346');
insert into model values (5,1,'Core i7-10700K', '2694');
insert into model values (6,1,'Core i9-10900K', '4695');
insert into model values (7,1,'Ryzen 9 3900X', '7388');
insert into model values (8,1,'Ryzen 5 3500X', '1766');
insert into model values (9,1,'Core 2 Duo E8400', '2096');
insert into model values (10,1,'Core i5-10400', '7659');
insert into model values (11,1,'AMD 3020e', '7977');
insert into model values (12,1,'AMD A10 Micro-6700T APU', '8355');
insert into model values (13,1,'AMD A10 PRO-7350B APU', '3513');
insert into model values (14,1,'AMD A10 PRO-7800B APU', '7591');
insert into model values (15,1,'AMD A10 PRO-7850B APU', '2646');
insert into model values (16,1,'AMD A10-4600M APU', '7470');
insert into model values (17,1,'AMD A10-4655M APU', '8897');
insert into model values (18,1,'AMD A10-4657M APU', '8742');
insert into model values (19,1,'AMD A1', '2910');
insert into model values (20,1,'', '8440');
insert into model values (21,2,'G.Skill Trident Z RGB ', '9573');
insert into model values (22,2,'Kingston HyperX Predator', '5104');
insert into model values (23,2,'Kingston HyperX Fury ', '3272');
insert into model values (24,2,'Corsair Dominator Platinum RGB ', '2996');
insert into model values (25,2,'HyperX Fury RGB 3733MHz – ', '4815');
insert into model values (26,2,'G.Skill Trident Z RGB DC', '1980');
insert into model values (27,2,'Adata Spectrix D80', '3428');
insert into model values (28,2,'G.Skill TridentZ Royal ', '6553');
insert into model values (29,2,'Corsair Vengeance LPX ', '6584');
insert into model values (30,3,'ASUS A7000', '3363');
insert into model values (31,3,'3DP Edition', '5982');
insert into model values (32,3,'9xx Soldiers sans frontiers Sigma 2', '5503');
insert into model values (33,3,'15FF', '3234');
insert into model values (34,3,'64MB DDR GeForce3 Ti 200', '9729');
insert into model values (35,3,'64MB GeForce2 MX with TV Out', '3960');
insert into model values (36,3,'128 DDR Radeon 9700 TX w/TV-Out', '7569');
insert into model values (37,3,'128 DDR Radeon 9800 Pro', '7179');
insert into model values (38,3,'128MB DDR Radeon 9800 Pro', '6564');
insert into model values (39,3,'128MB RADEON X600 SE', '6593');
insert into model values (40,3,'256MB DDR Radeon 9800 XT', '6223');
insert into model values (41,3,'256MB RADEON X600', '4034');
insert into model values (42,3,'7900 MOD - Radeon HD 6520G', '2934');
insert into model values (43,3,'7900 MOD - Radeon HD 6550D', '5575');
insert into model values (44,3,'A6 Micro-6500T Quad-Core APU with RadeonR4', '3628');
insert into model values (45,3,'A10-8700P', '3225');
insert into model values (46,3,'ABIT Siluro T400', '4703');
insert into model values (47,3,'ALL-IN-WONDER 9000', '2433');
insert into model values (48,3,'ALL-IN-WONDER 9800', '5864');
insert into model values (49,3,'ALL-IN-WONDER RADEON 8500DV', '4929');
insert into model values (50,4,'Adata XPG SX8200 Pro (1TB) ', '3759');
insert into model values (51,4,'SK hynix Gold P31 (1TB) ', '3271');
insert into model values (52,4,'Intel Optane SSD 905P (1TB) ', '9781');
insert into model values (53,4,'Sabrent Rocket NVMe 4.0 (2TB) ', '6091');
insert into model values (54,4,'Samsung 970 PRO (1TB) ', '9455');
insert into model values (55,4,'Silicon Power P34A80 (1TB)', '4072');
insert into model values (56,4,'Team Group T-Force Cardea Zero Z340', '5597');
insert into model values (57,5,'Seagate BarraCuda', '6201');
insert into model values (58,5,'Toshiba X300', '7403');
insert into model values (59,5,'WD VelociRaptor', '1115');
insert into model values (60,5,'WD Blue Desktop', '4563');
insert into model values (61,5,'Seagate Firecuda Desktop', '2402');
insert into model values (62,5,'Seagate IronWolf NAS', '3190');
insert into model values (63,5,'Seagate FireCuda Mobile', '3867');
insert into model values (64,6,'WD My Book', '5059');
insert into model values (65,6,'G-Technology G-Drive', '6456');
insert into model values (66,6,'Asus ROG Maximus XII Extreme', '4953');
insert into model values (67,6,' Gigabyte Z390 Aorus Ultra', '8455');
insert into model values (68,6,'Asus ROG Maximus XI Hero Wi-Fi', '5337');
insert into model values (69,6,'Asus ROG Strix Z390-I Gaming', '2595');
insert into model values (70,6,'MSI MPG X570 Gaming Pro Carbon WiFi.', '2592');
insert into model values (71,6,'Gigabyte X470 Aorus Gaming 5 Wi-Fi', '2190');
insert into model values (72,7,' Abkoncore | CR Platinum', '6667');
insert into model values (73,7,' ADATA / XPG | Core Reactor', '4336');
insert into model values (74,7,'Aerocool | Project 7', '7212');
insert into model values (75,7,'Andyson | N - R - PX - TX', '9667');
insert into model values (76,7,' Antec | High Current Gamer [HCG] Gold post 2018 y.', '9692');
insert into model values (77,7,'ASUS | ROG Strix post 2018 y. [1] / Thor', '7968');
insert into model values (78,7,'Chieftec / Chieftronic | Powerplay Gold / Platinum - GPS-500C Fanless', '4735');
insert into model values (79,7,' Cooler Master | Masterwatt Maker MiJ - V Gold 2013 [RS] Semi-modular [VSM] / Fully modular - V Platinum 2013 [RSC]', '3625');
insert into model values (80,7,'Corsair | AX Gold / Platinum / Titanium - RMx 2015 / 2018 - RM Gray - SF Gold / Platinum [2] - TXM 2017 (Gold)', '2683');
insert into model values (81,7,' Cougar | GX-F (Aurum) - LLC', '8017');
insert into model values (82,7,'Enermax | Platimax D.F <=600w - Maxtytan', '2916');
insert into model values (83,7,'EVGA | Supernova G2(L) / GS =>850W / P2 / PS / T2 (<=1200W/1600W)', '2520');
insert into model values (84,8,'Noctua NH-D15.', '7802');
insert into model values (85,8,'Cooler Master Hyper 212 RGB Black Edition.', '6215');
insert into model values (86,8,'Noctua NH-L9.', '9294');
insert into model values (87,8,'NZXT Kraken Z-3.', '9975');
insert into model values (88,8,'Corsair iCUE H115i Elite Capellix.', '3872');
insert into model values (89,8,'Cooler Master MasterLiquid ML240P Mirage.', '6169');
insert into model values (90,8,'Cooler Master MasterLiquid ML120R RGB.', '7431');
insert into model values (91,8,'Arctic Liquid Freezer 120.', '7380');
insert into model values (92,4,'ntel Optane 905P ', '4185');
insert into model values (93,4,'Samsung 970 Pro', '7133');
insert into model values (94,4,'Toshiba OCZ RD400 ', '3681');
insert into model values (95,4,'Adata XPG SX8200 SSD', '1649');
insert into model values (96,4,'Samsung 860 Pro', '6653');
insert into model values (97,4,'Intel 750 Series', '9983');
insert into model values (98,4,'Samsung 860 Evo ', '7877');
insert into model values (99,4,'HP S700 Pro ', '5162');
insert into model values (100,4,'Intel 760p Series SSD ', '8549');
insert into model values (101,4,'Samsung X5 Portable ', '6146');


/* INSERT QUERY NO: 1 */
INSERT INTO product(product_id, model_id, factory_id, product_name, product_description, price, quantity, creation_date)
VALUES
(
    1, 66, 1, 'Asus', '"Memory Max 128 GB Memory Type DDR4 RAID Support Yes"', 742.99, 85, '8/1/20'
);

/* INSERT QUERY NO: 2 */
INSERT INTO product(product_id, model_id, factory_id, product_name, product_description, price, quantity, creation_date)
VALUES
(
    2, 67, 1, 'Gigabyte', 'Memory Max 128 GB Memory Type DDR4 RAID Support Yes      ', 249.99, 48, '5/12/18'
);

/* INSERT QUERY NO: 3 */
INSERT INTO product(product_id, model_id, factory_id, product_name, product_description, price, quantity, creation_date)
VALUES
(
    3, 68, 1, 'Asus', 'Chipset Intel Z490         Memory Max 128 GB              Wireless Networking Wi-Fi 6', 399.99, 74, '8/15/20'
);

/* INSERT QUERY NO: 4 */
INSERT INTO product(product_id, model_id, factory_id, product_name, product_description, price, quantity, creation_date)
VALUES
(
    4, 69, 1, 'Asus', 'Form Factor Mini ITX            Memory Max 64 GB', 400.55, 14, '8/4/20'
);

/* INSERT QUERY NO: 5 */
INSERT INTO product(product_id, model_id, factory_id, product_name, product_description, price, quantity, creation_date)
VALUES
(
    5, 70, 1, 'MSI', '"Chipset AMD X570                                 Memory Max 128 GB"', 254.99, 145, '8/1/19'
);

/* INSERT QUERY NO: 6 */
INSERT INTO product(product_id, model_id, factory_id, product_name, product_description, price, quantity, creation_date)
VALUES
(
    6, 71, 1, 'Gigabyte', 'Memory Max 64 GB   Chipset AMD X470', 324.99, 18, '1/6/19'
);

/* INSERT QUERY NO: 7 */
INSERT INTO product(product_id, model_id, factory_id, product_name, product_description, price, quantity, creation_date)
VALUES
(
    7, 72, 1, 'Abkoncore', '"Modules 2 x 4GB"', 591.65, 85, '8/7/20'
);

/* INSERT QUERY NO: 8 */
INSERT INTO product(product_id, model_id, factory_id, product_name, product_description, price, quantity, creation_date)
VALUES
(
    8, 73, 1, 'ADATA', '"Efficiency Rating 80+Gold   Type ATX"', 159.99, 255, '8/28/20'
);

/* INSERT QUERY NO: 9 */
INSERT INTO product(product_id, model_id, factory_id, product_name, product_description, price, quantity, creation_date)
VALUES
(
    9, 74, 1, 'Aerocool', '"Efficiency Rating 80+ Platinum"', 170.99, 110, '7/9/20'
);

/* INSERT QUERY NO: 10 */
INSERT INTO product(product_id, model_id, factory_id, product_name, product_description, price, quantity, creation_date)
VALUES
(
    10, 75, 1, 'Andyson', 'Wattage 850 W Modular Full', 140.93, 108, '8/10/20'
);

/* INSERT QUERY NO: 11 */
INSERT INTO product(product_id, model_id, factory_id, product_name, product_description, price, quantity, creation_date)
VALUES
(
    11, 76, 1, 'Antec', 'Wattage 850 W Modular Full', 119.98, 58, '5/11/18'
);

/* INSERT QUERY NO: 12 */
INSERT INTO product(product_id, model_id, factory_id, product_name, product_description, price, quantity, creation_date)
VALUES
(
    12, 77, 1, 'Asus', 'Effective Memory Clock 14002 MHz   TDP 125 W', 120.99, 40, '8/12/18'
);

/* INSERT QUERY NO: 13 */
INSERT INTO product(product_id, model_id, factory_id, product_name, product_description, price, quantity, creation_date)
VALUES
(
    13, 78, 1, 'Chieftec', '"Efficiency Rating 80+  Wattage 600 W Type ATX"', 124.00, 114, '10/13/19'
);

/* INSERT QUERY NO: 14 */
INSERT INTO product(product_id, model_id, factory_id, product_name, product_description, price, quantity, creation_date)
VALUES
(
    14, 79, 1, 'Cooler Master', '"Wattage 500 W Modular No"', 130.99, 110, '8/14/13'
);

/* INSERT QUERY NO: 15 */
INSERT INTO product(product_id, model_id, factory_id, product_name, product_description, price, quantity, creation_date)
VALUES
(
    15, 80, 1, 'Corsair', 'PCIe 6+2-Pin Connectors 4        Efficiency > 90% SATA Connectors 12', 125.00, 85, '7/9/17'
);

/* INSERT QUERY NO: 16 */
INSERT INTO product(product_id, model_id, factory_id, product_name, product_description, price, quantity, creation_date)
VALUES
(
    16, 81, 1, 'Cougar', '"SATA Connectors 8 Molex 4-Pin Connectors 3"', 200.99, 97, '6/16/19'
);

/* INSERT QUERY NO: 17 */
INSERT INTO product(product_id, model_id, factory_id, product_name, product_description, price, quantity, creation_date)
VALUES
(
    17, 82, 1, 'Enermax', 'Wattage 1050 W  Fanless No                EPS Connectors 2', 210.99, 85, '8/9/18'
);

/* INSERT QUERY NO: 18 */
INSERT INTO product(product_id, model_id, factory_id, product_name, product_description, price, quantity, creation_date)
VALUES
(
    18, 83, 1, 'EVGA', 'Wattage 850 W Modular Full              EPS Connectors 2', 159.99, 110, '1/18/19'
);

/* INSERT QUERY NO: 19 */
INSERT INTO product(product_id, model_id, factory_id, product_name, product_description, price, quantity, creation_date)
VALUES
(
    19, 84, 1, 'Noctua', '"Noise Level 19.2 - 24.6 dB     Fanless  No           Water Cooled No"', 89.95, 74, '1/8/19'
);

/* INSERT QUERY NO: 20 */
INSERT INTO product(product_id, model_id, factory_id, product_name, product_description, price, quantity, creation_date)
VALUES
(
    20, 85, 1, 'Cooler Master', 'Fan RPM 650 - 2000 RPM                        Noise Level 8 - 30 dB ', 95.99, 110, '4/20/20'
);

/* INSERT QUERY NO: 21 */
INSERT INTO product(product_id, model_id, factory_id, product_name, product_description, price, quantity, creation_date)
VALUES
(
    21, 86, 1, 'Noctua', '"Fan RPM 600 - 2500 RPM"', 79.37, 102, '7/15/20'
);

/* INSERT QUERY NO: 22 */
INSERT INTO product(product_id, model_id, factory_id, product_name, product_description, price, quantity, creation_date)
VALUES
(
    22, 87, 1, 'NZXT', 'Fanless  No           Water Cooled No', 81.54, 95, '8/22/20'
);

/* INSERT QUERY NO: 23 */
INSERT INTO product(product_id, model_id, factory_id, product_name, product_description, price, quantity, creation_date)
VALUES
(
    23, 88, 1, 'Corsair', '"Fan RPM 2000 RPM Noise Level 10 - 36 dB"', 169.99, 85, '9/15/20'
);

/* INSERT QUERY NO: 24 */
INSERT INTO product(product_id, model_id, factory_id, product_name, product_description, price, quantity, creation_date)
VALUES
(
    24, 89, 1, 'Cooler Master', '"Fan RPM 650 - 1800 RPM   Noise Level 6 - 27 dB"', 64.98, 110, '4/21/20'
);

/* INSERT QUERY NO: 25 */
INSERT INTO product(product_id, model_id, factory_id, product_name, product_description, price, quantity, creation_date)
VALUES
(
    25, 90, 1, 'Cooler Master', '"Fan RPM 650 - 1800 RPM   Noise Level 6 - 27 dB"', 65.00, 104, '4/25/20'
);

/* INSERT QUERY NO: 26 */
INSERT INTO product(product_id, model_id, factory_id, product_name, product_description, price, quantity, creation_date)
VALUES
(
    26, 91, 1, 'Arctic', '"Fan RPM 500 - 1350 RPM"', 70.99, 70, '8/24/20'
);

/* INSERT QUERY NO: 27 */
INSERT INTO product(product_id, model_id, factory_id, product_name, product_description, price, quantity, creation_date)
VALUES
(
    27, 92, 1, 'Intel Optane', '"Capacity 380 GB                  NVME Yes"', 505.99, 110, '8/27/20'
);

/* INSERT QUERY NO: 28 */
INSERT INTO product(product_id, model_id, factory_id, product_name, product_description, price, quantity, creation_date)
VALUES
(
    28, 93, 1, 'Samsung', '"Form Factor M.2-2280  Type SSD Capacity 512 GB"', 169.99, 56, '8/8/07'
);

/* INSERT QUERY NO: 29 */
INSERT INTO product(product_id, model_id, factory_id, product_name, product_description, price, quantity, creation_date)
VALUES
(
    29, 94, 1, 'Toshiba', '"Capacity 480 GB Form Factor 2.5"""', 155.74, 57, '1/30/19'
);

/* INSERT QUERY NO: 30 */
INSERT INTO product(product_id, model_id, factory_id, product_name, product_description, price, quantity, creation_date)
VALUES
(
    30, 95, 1, 'Adata', '"Capacity 480 GB"', 101.38, 80, '11/28/18'
);

/* INSERT QUERY NO: 31 */
INSERT INTO product(product_id, model_id, factory_id, product_name, product_description, price, quantity, creation_date)
VALUES
(
    31, 96, 1, 'Samsung', '"Capacity 256 GB    Cache 512 MB"', 87.99, 84, '10/21/20'
);

/* INSERT QUERY NO: 32 */
INSERT INTO product(product_id, model_id, factory_id, product_name, product_description, price, quantity, creation_date)
VALUES
(
    32, 97, 1, 'Intel', '"Capacity 1.2 TB"', 90.99, 42, '12/1/19'
);

/* INSERT QUERY NO: 33 */
INSERT INTO product(product_id, model_id, factory_id, product_name, product_description, price, quantity, creation_date)
VALUES
(
    33, 98, 1, 'Samsung', '"Cache 512 MB"', 49.99, 36, '7/12/18'
);

/* INSERT QUERY NO: 34 */
INSERT INTO product(product_id, model_id, factory_id, product_name, product_description, price, quantity, creation_date)
VALUES
(
    34, 99, 1, 'HP', '"Capacity 500 GB"', 52.99, 24, '4/3/19'
);

/* INSERT QUERY NO: 35 */
INSERT INTO product(product_id, model_id, factory_id, product_name, product_description, price, quantity, creation_date)
VALUES
(
    35, 100, 1, 'Intel', '"Capacity 1 TB"', 78.99, 21, '12/7/17'
);

/* INSERT QUERY NO: 36 */
INSERT INTO product(product_id, model_id, factory_id, product_name, product_description, price, quantity, creation_date)
VALUES
(
    36, 101, 1, 'Samsung', '"Capacity 1 TB"', 100.99, 105, '2/2/10'
);

/* INSERT QUERY NO: 37 */
INSERT INTO product(product_id, model_id, factory_id, product_name, product_description, price, quantity, creation_date)
VALUES
(
    37, 65, 1, 'G-Technology', '"Capacity 4000 GB   RPM 7200RPM"', 400.99, 26, '9/15/19'
);

/* INSERT QUERY NO: 38 */
INSERT INTO product(product_id, model_id, factory_id, product_name, product_description, price, quantity, creation_date)
VALUES
(
    38, 64, 1, 'WD', '"Capacity 2000 GB"', 418.99, 67, '12/14/19'
);

/* INSERT QUERY NO: 39 */
INSERT INTO product(product_id, model_id, factory_id, product_name, product_description, price, quantity, creation_date)
VALUES
(
    39, 63, 1, 'Seagate', '"Capacity 2 TB"', 159.99, 95, '4/14/18'
);

/* INSERT QUERY NO: 40 */
INSERT INTO product(product_id, model_id, factory_id, product_name, product_description, price, quantity, creation_date)
VALUES
(
    40, 62, 1, 'Seagate', '"Capacity 16 TB  Cache 256 MB"', 464.00, 76, '12/9/19'
);

/* INSERT QUERY NO: 41 */
INSERT INTO product(product_id, model_id, factory_id, product_name, product_description, price, quantity, creation_date)
VALUES
(
    41, 61, 1, 'Seagate', '"Capacity 2 TB"', 160.99, 84, '4/14/20'
);

/* INSERT QUERY NO: 42 */
INSERT INTO product(product_id, model_id, factory_id, product_name, product_description, price, quantity, creation_date)
VALUES
(
    42, 60, 1, 'WD', '"Cache 64 MB"', 205.99, 105, '8/2/18'
);

/* INSERT QUERY NO: 43 */
INSERT INTO product(product_id, model_id, factory_id, product_name, product_description, price, quantity, creation_date)
VALUES
(
    43, 59, 1, 'WD', '"Capacity 1 TB"', 150.99, 65, '11/22/20'
);

/* INSERT QUERY NO: 44 */
INSERT INTO product(product_id, model_id, factory_id, product_name, product_description, price, quantity, creation_date)
VALUES
(
    44, 58, 1, 'Toshiba', '"Capacity 5 TB"', 160.00, 45, '4/15/17'
);

/* INSERT QUERY NO: 45 */
INSERT INTO product(product_id, model_id, factory_id, product_name, product_description, price, quantity, creation_date)
VALUES
(
    45, 57, 1, 'Seagate BarraCuda', '"Cache 32 MB  "', 163.59, 53, '9/14/19'
);

/* INSERT QUERY NO: 46 */
INSERT INTO product(product_id, model_id, factory_id, product_name, product_description, price, quantity, creation_date)
VALUES
(
    46, 56, 1, 'Team', '"Interface M.2 (M)"', 150.99, 64, '7/1/15'
);

/* INSERT QUERY NO: 47 */
INSERT INTO product(product_id, model_id, factory_id, product_name, product_description, price, quantity, creation_date)
VALUES
(
    47, 55, 1, 'Silicon Power', '"Interface SATA 6 Gb/s"', 89.99, 94, '4/17/18'
);

/* INSERT QUERY NO: 48 */
INSERT INTO product(product_id, model_id, factory_id, product_name, product_description, price, quantity, creation_date)
VALUES
(
    48, 54, 1, 'Samsung', '"Cache 1024 MB"', 349.41, 87, '6/15/20'
);

/* INSERT QUERY NO: 49 */
INSERT INTO product(product_id, model_id, factory_id, product_name, product_description, price, quantity, creation_date)
VALUES
(
    49, 53, 1, 'Sabrent', '"Form Factor M.2-2280"', 399.99, 85, '6/30/18'
);

/* INSERT QUERY NO: 50 */
INSERT INTO product(product_id, model_id, factory_id, product_name, product_description, price, quantity, creation_date)
VALUES
(
    50, 52, 1, 'Intel', '"Interface U.2"', 2228.99, 74, '3/19/20'
);

/* INSERT QUERY NO: 51 */
INSERT INTO product(product_id, model_id, factory_id, product_name, product_description, price, quantity, creation_date)
VALUES
(
    51, 51, 1, 'SK hynix', '"Form Factor M.2-2280"', 134.99, 57, '5/6/19'
);

/* INSERT QUERY NO: 52 */
INSERT INTO product(product_id, model_id, factory_id, product_name, product_description, price, quantity, creation_date)
VALUES
(
    52, 50, 1, 'Adata', '"Form Factor M.2-2280"', 149.99, 24, '1/21/20'
);

/* INSERT QUERY NO: 53 */
INSERT INTO product(product_id, model_id, factory_id, product_name, product_description, price, quantity, creation_date)
VALUES
(
    53, 49, 1, 'ATI', '"GPU Clock 230 MHz  Memory Clock 190 MHz 380 Mbps effective "', 250.99, 11, '9/17/01'
);

/* INSERT QUERY NO: 54 */
INSERT INTO product(product_id, model_id, factory_id, product_name, product_description, price, quantity, creation_date)
VALUES
(
    54, 48, 1, 'ATI', 'Graphics Ram Size 128 MB  Graphics Ram Type SDRAM', 399.98, 45, '7/20/20'
);

/* INSERT QUERY NO: 55 */
INSERT INTO product(product_id, model_id, factory_id, product_name, product_description, price, quantity, creation_date)
VALUES
(
    55, 47, 1, 'ATI', '"Bus width: 128 bit    GPU Clock 275 MHz"', 500.99, 24, '6/14/19'
);

/* INSERT QUERY NO: 56 */
INSERT INTO product(product_id, model_id, factory_id, product_name, product_description, price, quantity, creation_date)
VALUES
(
    56, 46, 1, 'Abit', 'Chipset: Geforce2 MX400   Memory size: 64 MB', 200.99, 56, '4/24/20'
);

/* INSERT QUERY NO: 57 */
INSERT INTO product(product_id, model_id, factory_id, product_name, product_description, price, quantity, creation_date)
VALUES
(
    57, 45, 1, 'AMD', 'Socket Socket FP4', 339.99, 84, '7/16/15'
);

/* INSERT QUERY NO: 58 */
INSERT INTO product(product_id, model_id, factory_id, product_name, product_description, price, quantity, creation_date)
VALUES
(
    58, 44, 1, 'AMD ', 'Clockspeed: 1.2 GHz', 410.99, 87, '4/5/20'
);

/* INSERT QUERY NO: 59 */
INSERT INTO product(product_id, model_id, factory_id, product_name, product_description, price, quantity, creation_date)
VALUES
(
    59, 43, 1, 'AMD', 'Core Speed 600 MHz', 520.99, 92, '9/28/13'
);

/* INSERT QUERY NO: 60 */
INSERT INTO product(product_id, model_id, factory_id, product_name, product_description, price, quantity, creation_date)
VALUES
(
    60, 42, 1, 'AMD', 'Core Speed 400 MHz', 128.56, 54, '11/10/17'
);

/* INSERT QUERY NO: 61 */
INSERT INTO product(product_id, model_id, factory_id, product_name, product_description, price, quantity, creation_date)
VALUES
(
    61, 41, 1, 'AMD', 'Memory Frequency 300MHz', 300.55, 30, '7/28/19'
);

/* INSERT QUERY NO: 62 */
INSERT INTO product(product_id, model_id, factory_id, product_name, product_description, price, quantity, creation_date)
VALUES
(
    62, 40, 1, 'AMD', 'Core R350   Cores frequency:  412MHz', 500.99, 47, '10/15/18'
);

/* INSERT QUERY NO: 63 */
INSERT INTO product(product_id, model_id, factory_id, product_name, product_description, price, quantity, creation_date)
VALUES
(
63, 39, 1, 'AMD', 'Graphics Processor: RV370', 320.22, 48, '2/5/20'
);

/* INSERT QUERY NO: 64 */
INSERT INTO product(product_id, model_id, factory_id, product_name, product_description, price, quantity, creation_date)
VALUES
(
64, 38, 1, 'AMD', 'Core: R350 / 360', 345.20, 85, '11/12/19'
);

/* INSERT QUERY NO: 65 */
INSERT INTO product(product_id, model_id, factory_id, product_name, product_description, price, quantity, creation_date)
VALUES
(
65, 37, 1, 'AMD', 'Interface:AGP 8x/4x', 142.55, 83, '5/4/18'
);

/* INSERT QUERY NO: 66 */
INSERT INTO product(product_id, model_id, factory_id, product_name, product_description, price, quantity, creation_date)
VALUES
(
66, 36, 1, 'AMD', 'Pixel Shaders 2.0', 255.20, 64, '10/19/17'
);

/* INSERT QUERY NO: 67 */
INSERT INTO product(product_id, model_id, factory_id, product_name, product_description, price, quantity, creation_date)
VALUES
(
67, 35, 1, 'Inno3D', 'Interface TypeAGP 4x  Bus TypeAGP 4x', 241.36, 32, '7/6/20'
);

/* INSERT QUERY NO: 68 */
INSERT INTO product(product_id, model_id, factory_id, product_name, product_description, price, quantity, creation_date)
VALUES
(
68, 34, 1, ' NVIDIA', 'Core: NV20', 300.25, 59, '7/7/19'
);

/* INSERT QUERY NO: 69 */
INSERT INTO product(product_id, model_id, factory_id, product_name, product_description, price, quantity, creation_date)
VALUES
(
69, 33, 1, 'MSI', '"Memory Type GDDR6"', 400.25, 6, '10/8/14'
);

/* INSERT QUERY NO: 70 */
INSERT INTO product(product_id, model_id, factory_id, product_name, product_description, price, quantity, creation_date)
VALUES
(
70, 32, 1, 'Sigma', '"Boost Clock 1815 MHz"', 239.00, 14, '4/17/20'
);

/* INSERT QUERY NO: 71 */
INSERT INTO product(product_id, model_id, factory_id, product_name, product_description, price, quantity, creation_date)
VALUES
(
71, 31, 1, ' NVIDIA', '"Core Clock 1605 MHz"', 499.95, 67, '10/10/02'
);

/* INSERT QUERY NO: 72 */
INSERT INTO product(product_id, model_id, factory_id, product_name, product_description, price, quantity, creation_date)
VALUES
(
72, 30, 1, 'ASUS', '"Boost Clock 1770 MHz"', 600.55, 15, '11/11/17'
);

/* INSERT QUERY NO: 73 */
INSERT INTO product(product_id, model_id, factory_id, product_name, product_description, price, quantity, creation_date)
VALUES
(
73, 29, 1, 'Corsair', '"Form Factor 288-pin DIMM"', 62.99, 58, '10/1/15'
);

/* INSERT QUERY NO: 74 */
INSERT INTO product(product_id, model_id, factory_id, product_name, product_description, price, quantity, creation_date)
VALUES
(
74, 28, 1, 'G.Skill', '"Form Factor 240-pin DIMM  "', 100.85, 75, '5/21/20'
);

/* INSERT QUERY NO: 75 */
INSERT INTO product(product_id, model_id, factory_id, product_name, product_description, price, quantity, creation_date)
VALUES
(
75, 27, 1, 'Adata', '"Modules 2 x 8GB"', 222.36, 91, '4/10/19'
);

/* INSERT QUERY NO: 76 */
INSERT INTO product(product_id, model_id, factory_id, product_name, product_description, price, quantity, creation_date)
VALUES
(
76, 26, 1, 'G.Skill', '"First Word Latency 9.194 ns"', 200.15, 98, '4/11/18'
);

/* INSERT QUERY NO: 77 */
INSERT INTO product(product_id, model_id, factory_id, product_name, product_description, price, quantity, creation_date)
VALUES
(
77, 25, 1, 'Kingston', '"Form Factor 288-pin DIMM  CAS Latency 19"', 240.94, 106, '4/14/18'
);

/* INSERT QUERY NO: 78 */
INSERT INTO product(product_id, model_id, factory_id, product_name, product_description, price, quantity, creation_date)
VALUES
(
78, 24, 1, 'Corsair', '"Form Factor 288-pin DIMM  First Word Latency 10 ns"', 1599.99, 27, '10/17/19'
);

/* INSERT QUERY NO: 79 */
INSERT INTO product(product_id, model_id, factory_id, product_name, product_description, price, quantity, creation_date)
VALUES
(
79, 23, 1, 'Kingston', '"Modules 2 x 8GB First Word Latency 12.003 ns"', 512.95, 39, '4/29/18'
);

/* INSERT QUERY NO: 80 */
INSERT INTO product(product_id, model_id, factory_id, product_name, product_description, price, quantity, creation_date)
VALUES
(
80, 22, 1, 'Kingston', '"Form Factor 240-pin DIMM  First Word Latency 10.718 ns"', 600.84, 48, '4/14/17'
);

/* INSERT QUERY NO: 81 */
INSERT INTO product(product_id, model_id, factory_id, product_name, product_description, price, quantity, creation_date)
VALUES
(
81, 21, 1, 'G.Skill', '"First Word Latency 8 ns  CAS Latency 16"', 159.98, 99, '5/24/19'
);

/* INSERT QUERY NO: 82 */
INSERT INTO product(product_id, model_id, factory_id, product_name, product_description, price, quantity, creation_date)
VALUES
(
82, 20, 1, 'G.Skill', '"First Word Latency 12.5 ns"', 200.67, 49, '2/21/18'
);

/* INSERT QUERY NO: 83 */
INSERT INTO product(product_id, model_id, factory_id, product_name, product_description, price, quantity, creation_date)
VALUES
(
83, 19, 1, 'AMD', 'Clockspeed: 2.3 GHz Frequency    2100 MHz', 400.54, 28, '7/21/20'
);

/* INSERT QUERY NO: 84 */
INSERT INTO product(product_id, model_id, factory_id, product_name, product_description, price, quantity, creation_date)
VALUES
(
84, 18, 1, 'AMD', 'Cache 4096 KB', 350.99, 17, '9/14/20'
);

/* INSERT QUERY NO: 85 */
INSERT INTO product(product_id, model_id, factory_id, product_name, product_description, price, quantity, creation_date)
VALUES
(
85, 17, 1, 'AMD', 'Techprocessor:32 nm', 450.85, 19, '6/30/18'
);

/* INSERT QUERY NO: 86 */
INSERT INTO product(product_id, model_id, factory_id, product_name, product_description, price, quantity, creation_date)
VALUES
(
86, 16, 1, 'AMD', 'Frequency    3200 MHz', 540.65, 56, '5/12/18'
);

/* INSERT QUERY NO: 87 */
INSERT INTO product(product_id, model_id, factory_id, product_name, product_description, price, quantity, creation_date)
VALUES
(
87, 15, 1, 'AMD', '"ECC Support No"', 300.99, 19, '4/14/20'
);

/* INSERT QUERY NO: 88 */
INSERT INTO product(product_id, model_id, factory_id, product_name, product_description, price, quantity, creation_date)
VALUES
(
88, 14, 1, 'AMD', '"TDP 65 W"', 195.99, 46, '1/27/19'
);

/* INSERT QUERY NO: 89 */
INSERT INTO product(product_id, model_id, factory_id, product_name, product_description, price, quantity, creation_date)
VALUES
(
89, 13, 1, 'AMD', '"L3 Cache 1 x 8 MB"', 152.99, 25, '2/19/19'
);

/* INSERT QUERY NO: 90 */
INSERT INTO product(product_id, model_id, factory_id, product_name, product_description, price, quantity, creation_date)
VALUES
(
90, 12, 1, 'AMD', '"Boost Clock 3.4 GHz"', 200.99, 63, '1/19/20'
);

/* INSERT QUERY NO: 91 */
INSERT INTO product(product_id, model_id, factory_id, product_name, product_description, price, quantity, creation_date)
VALUES
(
91, 11, 1, 'AMD', '"TDP 65 W"', 179.99, 31, '10/1/20'
);

/* INSERT QUERY NO: 92 */
INSERT INTO product(product_id, model_id, factory_id, product_name, product_description, price, quantity, creation_date)
VALUES
(
92, 10, 1, 'Intel', '"TDP 65 W Boost Clock 4.3 GHz  "', 181.25, 29, '7/20/20'
);

/* INSERT QUERY NO: 93 */
INSERT INTO product(product_id, model_id, factory_id, product_name, product_description, price, quantity, creation_date)
VALUES
(
93, 9, 1, 'Intel', '"Core Clock 3 GHz Socket LGA775"', 18.43, 45, '11/1/18'
);

/* INSERT QUERY NO: 94 */
INSERT INTO product(product_id, model_id, factory_id, product_name, product_description, price, quantity, creation_date)
VALUES
(
94, 8, 1, 'AMD', '"TDP 65 W"', 600.99, 48, '10/2/19'
);

/* INSERT QUERY NO: 95 */
INSERT INTO product(product_id, model_id, factory_id, product_name, product_description, price, quantity, creation_date)
VALUES
(
95, 7, 1, 'AMD', '"TDP 105 W"', 429.99, 80, '11/23/18'
);

/* INSERT QUERY NO: 96 */
INSERT INTO product(product_id, model_id, factory_id, product_name, product_description, price, quantity, creation_date)
VALUES
(
96, 6, 1, 'Intel', '"Core Clock 3.7 GHz Socket LGA1200"', 551.25, 100, '7/4/20'
);

/* INSERT QUERY NO: 97 */
INSERT INTO product(product_id, model_id, factory_id, product_name, product_description, price, quantity, creation_date)
VALUES
(
97, 5, 1, 'Intel', '"Core Count 8 Core Clock 3.8 GHz"', 377.77, 100, '4/1/20'
);

/* INSERT QUERY NO: 98 */
INSERT INTO product(product_id, model_id, factory_id, product_name, product_description, price, quantity, creation_date)
VALUES
(
98, 4, 0, 'AMD', '"Core Clock 3.5 GHz TDP 65 W"', 180.99, 80, '10/5/20'
);

/* INSERT QUERY NO: 99 */
INSERT INTO product(product_id, model_id, factory_id, product_name, product_description, price, quantity, creation_date)
VALUES
(
99, 3, 1, 'AMD', '"TDP 65 W"', 294.99, 49, '5/9/20'
);

/* INSERT QUERY NO: 100 */
INSERT INTO product(product_id, model_id, factory_id, product_name, product_description, price, quantity, creation_date)
VALUES
(
100, 2, 1, 'AMD', '"Core Clock 3.6 GHz Boost Clock 3.9 GHz"', 105.95, 95, '11/4/17'
);

/* INSERT QUERY NO: 101 */
INSERT INTO product(product_id, model_id, factory_id, product_name, product_description, price, quantity, creation_date)
VALUES
(
101, 1, 1, 'AMD', '"Core Clock 3.6 GHz"', 199.0, 84, '7/9/19'
);


